//
//  ildts5wtchApp.swift
//  ildts5wtch Watch App
//
//  Created by Josemaria Carazo Abolafia on 28/3/23.
//

import SwiftUI

@main
struct ildts5wtch_Watch_AppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
