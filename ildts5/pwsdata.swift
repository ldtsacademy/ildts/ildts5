//
//  pwsdata.swift
//  ildts5
//
//  Created by Josemaria Carazo Abolafia on 28/3/23.
//

import Foundation

class ResponseModelW: Codable, Identifiable {
    var observations: [Observation?]
    init(observations: [Observation]?) {
        self.observations = [Observation?](repeating: nil, count: 1)
        self.appendvoidobs()
    }
    
    func appendvoidobs() {
        let m0: MetricModel = MetricModel.init(temp: 0, heatIndex: 0, dewpt: 0, windChill: 0, windSpeed: 0, windGust: 0, pressure: 0, precipRate: 0, precipTotal: 0, elev: 0)
        let jn: JSONNull = JSONNull()
        let obs0 = Observation.init(stationID: "", obsTimeUTC: "", obsTimeLocal: "", neighborhood: "", softwareType: "", country: "", solarRadiation: 0, lon: 0, realtimeFrequency: jn, epoch: 0, lat: 0, uv: 0, winddir: 0, humidity: 0, qcStatus: 0, metric: m0)
        self.observations.append(obs0)
    }
    func returnvoidobs() -> Observation {
        let m0: MetricModel = MetricModel.init(temp: 0, heatIndex: 0, dewpt: 0, windChill: 0, windSpeed: 0, windGust: 0, pressure: 0, precipRate: 0, precipTotal: 0, elev: 0)
        let jn: JSONNull = JSONNull()
        let obs0 = Observation.init(stationID: "", obsTimeUTC: "", obsTimeLocal: "", neighborhood: "", softwareType: "", country: "", solarRadiation: 0, lon: 0, realtimeFrequency: jn, epoch: 0, lat: 0, uv: 0, winddir: 0, humidity: 0, qcStatus: 0, metric: m0)
        return obs0
    }
}

// MARK: - Observation
class Observation: Codable, Identifiable {

    var stationID: String? = ""
    var obsTimeUTC: String? = ""
    var obsTimeLocal: String? = ""
    var neighborhood: String? = ""
    var softwareType: String? = ""
    var country: String? = ""
    var solarRadiation: Float? = 0
    var lon: Double? = 0
    var realtimeFrequency: JSONNull? = JSONNull()
    var epoch: Int? = 0
    var lat: Double? = 0
    var uv: Float? = 0
    var winddir: Int? = 0
    var humidity: Int? = 0
    var qcStatus: Int? = 0
    var metric: MetricModel
    
    init(stationID: String? = nil, obsTimeUTC: String? = nil, obsTimeLocal: String? = nil, neighborhood: String? = nil, softwareType: String? = nil, country: String? = nil, solarRadiation: Float? = nil, lon: Double? = nil, realtimeFrequency: JSONNull? = nil, epoch: Int? = nil, lat: Double? = nil, uv: Float? = nil, winddir: Int? = nil, humidity: Int? = nil, qcStatus: Int? = nil, metric: MetricModel) {
        self.stationID = stationID
        self.obsTimeUTC = obsTimeUTC
        self.obsTimeLocal = obsTimeLocal
        self.neighborhood = neighborhood
        self.softwareType = softwareType
        self.country = country
        self.solarRadiation = solarRadiation
        self.lon = lon
        self.realtimeFrequency = realtimeFrequency
        self.epoch = epoch
        self.lat = lat
        self.uv = uv
        self.winddir = winddir
        self.humidity = humidity
        self.qcStatus = qcStatus
        self.metric = metric
    }
    
    func Observation() {
        let m0: MetricModel = MetricModel.init(temp: 0, heatIndex: 0, dewpt: 0, windChill: 0, windSpeed: 0, windGust: 0, pressure: 0, precipRate: 0, precipTotal: 0, elev: 0)
        metric = m0
    }

}

class MetricModel: Codable, Identifiable {
    var temp, heatIndex, dewpt, windChill: Int?
    var windSpeed, windGust: Int?
    var pressure, precipRate, precipTotal: Float?
    var elev: Int?
    
    init(temp: Int?, heatIndex: Int?, dewpt: Int?, windChill: Int?, windSpeed: Int?, windGust: Int?, pressure: Float?, precipRate: Float?, precipTotal: Float?, elev: Int?) {
        self.temp = temp
        self.heatIndex = heatIndex
        self.dewpt = dewpt
        self.windChill = windChill
        self.windSpeed = windSpeed
        self.windGust = windGust
        self.pressure = pressure
        self.precipRate = precipRate
        self.precipTotal = precipTotal
        self.elev = elev
    }
    
    func MetricModel() {
        self.temp = 0
        self.heatIndex = 0
        self.dewpt = 0
        self.windChill = 0
        self.windSpeed = 0
        self.windGust = 0
        self.pressure = 0
        self.precipRate = 0
        self.precipTotal = 0
        self.elev = 0
    }

}


class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        var container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}

