#  Title

//
//  pwsdata.swift
//  ildts5
//
//  Created by Josemaria Carazo Abolafia on 28/3/23.
//

import Foundation

class pwsdata: Codable, Identifiable {
    let observations: [Observation]?
    
    init(observations: [Observation]?) {
        self.observations = observations
    }
}

// MARK: - Observation
class Observation: Codable, Identifiable, Hashable {

    let stationID: String?
    let obsTimeUTC: String?
    let obsTimeLocal, neighborhood, softwareType, country: String?
    let solarRadiation: Float?
    let lon: Double?
    let realtimeFrequency: JSONNull?
    let epoch: Int?
    let lat: Double?
    let uv: Float?
    let winddir, humidity, qcStatus: Int?
    let metric: MetricModel?
    
    init(stationID: String?, obsTimeUTC: String?, obsTimeLocal: String?, neighborhood: String?, softwareType: String?, country: String?, solarRadiation: Float?, lon: Double?, realtimeFrequency: JSONNull?, epoch: Int?, lat: Double?, uv: Float?, winddir: Int?, humidity: Int?, qcStatus: Int?, metric: MetricModel?) {
        self.stationID = stationID
        self.obsTimeUTC = obsTimeUTC
        self.obsTimeLocal = obsTimeLocal
        self.neighborhood = neighborhood
        self.softwareType = softwareType
        self.country = country
        self.solarRadiation = solarRadiation
        self.lon = lon
        self.realtimeFrequency = realtimeFrequency
        self.epoch = epoch
        self.lat = lat
        self.uv = uv
        self.winddir = winddir
        self.humidity = humidity
        self.qcStatus = qcStatus
        self.metric = metric
    }

    static func == (lhs: Observation, rhs: Observation) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }
    
}

class MetricModel: Codable, Identifiable {
    let temp, heatIndex, dewpt, windChill: Int?
    let windSpeed, windGust: Int?
    let pressure, precipRate, precipTotal: Float?
    let elev: Int?
    
    init(temp: Int?, heatIndex: Int?, dewpt: Int?, windChill: Int?, windSpeed: Int?, windGust: Int?, pressure: Float?, precipRate: Float?, precipTotal: Float?, elev: Int?) {
        self.temp = temp
        self.heatIndex = heatIndex
        self.dewpt = dewpt
        self.windChill = windChill
        self.windSpeed = windSpeed
        self.windGust = windGust
        self.pressure = pressure
        self.precipRate = precipRate
        self.precipTotal = precipTotal
        self.elev = elev
    }
}


class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}

