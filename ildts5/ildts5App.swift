//
//  ildts5App.swift
//  ildts5
//
//  Created by Josemaria Carazo Abolafia on 28/3/23.
//

import SwiftUI

@main
struct ildts5App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
