//
//  ResponseModel.swift
//  ildts5
//
//  Created by Josemaria Carazo Abolafia on 28/3/23.
//

import Foundation

class ResponseModel: Codable, Identifiable {
    //ESTE ES EL MODELO CON EL QUE DECODIFICARE EL JSON QUE RECIBA POR HTTPS...
    //TIENE QUE CASAR CON LA QUERY Y EL OBJETO JSON DE LA API EN EL SERVIDOR HTTPS
    var idGrupo: String? = ""
    var pec: String? = ""
    var clase: String? = ""
    var Profe: ProfeModel
    
    class ProfeModel: Codable, Identifiable {
        //ESTE ES EL MODELO CON EL QUE DECODIFICARE EL JSON QUE RECIBA POR HTTPS...
        //TIENE QUE CASAR CON LA QUERY Y EL OBJETO JSON DE LA API EN EL SERVIDOR HTTPS
        var profeid: Float = 0
    }

    
}
