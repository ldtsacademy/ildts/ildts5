//
//  ContentView.swift
//  ildts5
//
//  Created by LDTS Ltd. Liability Company on 23/2/23.
//  Todos los derechos reservados.
//

import SwiftUI

struct ContentView: View {
            
    @State var models: [ResponseModelW] = []
    @State var modelo1: ResponseModelW?

    let webaddr = "https://api.weather.com/v2/pws/observations/current?stationId=IJAN47&format=json&units=m&apiKey=dacb05b6525345308b05b6525315307f"
    //let webaddr = "https://lab.ldts.us/phplab/pru1.php"
    
    var body: some View {
        
        Section {
            Label("LDTS Weather", systemImage: "cloud.sun")
                .font(.subheadline)
            Label("by LDTS", systemImage: "signature")
                .font(.footnote)
        }
        
        Divider().padding(.top, 1.0).padding(.bottom, 1.0).foregroundColor(/*@START_MENU_TOKEN@*/.blue/*@END_MENU_TOKEN@*/)
        
        //AQUI ESTA EL CONTENIDO PRINCIPAL: LA LISTA VERTICAL
        VStack {
            
            //ANTES DE MOSTRAR ESTA VSTACK SE EJECUTARA EL onAppear QUE ESTA DEBAJO...
            List (self.models) { (model) in
                VStack{
                    let t1 = model.observations[0]?.neighborhood ?? ""
                    let t1b = model.observations[0]?.country ?? ""
                    let t1c = model.observations[0]?.lat ?? 0
                    let t1d = model.observations[0]?.lon ?? 0
                    let t2 = model.observations[0]?.metric.temp ?? 0
                    let t3 = model.observations[0]?.solarRadiation ?? 0
                    let t4 = model.observations[0]?.metric.windSpeed ?? 0
                    let t5 = model.observations[0]?.winddir ?? 0
                    let t6 = model.observations[0]?.humidity ?? 0
                    HStack{
                        Text(t1 + " | " + t1b).bold()
                        Text(" ( \(t1c) , \(t1d) )").fontWeight(Font.Weight.light)
                    }.padding(.bottom, 2)
                    Text("\(t2) ºC").italic().font(Font.largeTitle)
                    Text("Luz: " + "\(t3)").italic().font(Font.footnote)
                    Text("Viento: " + "\(t4)Km/h  \(t5)º").italic().font(Font.footnote)
                    Text("Humedad: " + "\(t6)%").italic().font(Font.footnote)
                }
            }.refreshable {
                //ESTO SE EJECUTA CUANDO ARRASTRO LA LISTA HACIA ABAJO...
                loaddata()
            }
            
        }.onAppear(perform: {
            //ESTO SE EJECUTA ANTES DE QUE APAREZCA EL VStack
            loaddata()
        })
        .padding()
        
    }
    
    func loaddata() {
        //ESTA FUNCIÓN CONECTA CON LA API POR HTTPS Y RECIBE LA INFO EN JSON
        //PARA QUE PUEDA USAR LAS VARIABLE models Y SER LLAMADA DESDE body,
        //TIENE QUE DEFINIRSE DENTRO DEL MISMO STRUCT.
        guard let url: URL = URL(string: webaddr) else {
            print("HTTPS Server Connection Error")
            return
        }
        
        var urlRequest: URLRequest =
        URLRequest(url: url)
        urlRequest.httpMethod = "GET"
        
        
        //A CONTINUACIÓN CONECTAMOS...
        URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
            guard let data = data else {
                print("Invalid HTTPS Response")
                return
            }
            
            //SI HA CONECTADO BIEN Y SE HA RECIBIDO LOS DATOS EN JSON EN LA VARIABLE data...
            do {
                //self.models = ResponseModelW
                var modeloaux: ResponseModelW = ResponseModelW.init(observations: nil)
                modeloaux.appendvoidobs()
                modeloaux = try  //...INTENTO DECODIFICAR EL JSON QUE DEBE CUADRAR CON EL MODELO ResponseModel
                JSONDecoder().decode(
                    ResponseModelW.self, from: data)
                self.models.append(modeloaux)
            } catch {
                print(error.localizedDescription)
            }
        }).resume()
    }
    
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

